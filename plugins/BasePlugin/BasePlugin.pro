#-------------------------------------------------
#
# Project created by QtCreator 2017-04-24T15:33:36
#
#-------------------------------------------------

QT       += core gui widgets

TARGET = ../build/plugins/baseplugin
TEMPLATE = lib
CONFIG += plugin

SOURCES += \
    baseplugin.cpp

HEADERS  += ../../src/plugins/isettingspage.h ../../src/plugins/iapplicationplugin.h ../../src/plugins/iwidgetpage.h \
    baseplugin.h

RESOURCES += \
    ../../assets/icons.qrc

